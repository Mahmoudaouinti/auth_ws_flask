virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt


#Run server at port 5000
FLASK_APP=run.py flask run